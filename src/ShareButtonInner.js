import React from 'react';
import {
    Text, View,
} from 'react-native';


export default class ShareButtonInner extends React.Component {

    async onPress() {
        await this.props.client.reactions.add('share', this.props.activity);
    }

    render() {
        return (

            < View >
                <Text onPress={this.onPress}>Share</Text>
            </View >
        )

    }

}