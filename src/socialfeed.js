
import React, { Component } from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  TouchableOpacity,
  
} from 'react-native';
import moment from 'moment';
import { StreamApp, updateStyle, FlatFeed, Activity, LikeButton, StatusUpdateForm,FollowButton } from 'react-native-activity-feed';


import Report from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/dist/EvilIcons';
import ShareIcon from 'react-native-vector-icons/Feather';


export default class Socialfeed extends Component {

  constructor(props) {
    super(props);

    updateStyle('flatFeed', {
      container: {
        backgroundColor: '#E5E5E5',

      }
    });

    updateStyle('activity', {
      header: {
        padding: 0,
      },
      container: {
        backgroundColor: 'white',
        borderRadius: 10,
        paddingTop: 6,
        paddingBottom: 5
      }
    });

    updateStyle('likeButton', {
      text: {
        color: 'black'
      },
      container: {
        marginLeft: 10,
        paddingTop: 10,
        paddingBottom: 0,
        backgroundColor: 'white',
        alignItems: 'center',
        height: 50,
      },
      image: {
        width: 30,
        height: 30
      }

    })
    this.CustomActivity.bind(this);
    this.reportview.bind(this);
    this.commentclick.bind(this);
    this.descriptionclick.bind(this);
  }

  reportview = () => {
    console.log("report click --->")

    this.props.navigation.navigate("Post")

  }
  commentclick = (activity) => {
    console.log("commentclick--->")
    this.props.navigation.navigate("List", { activity })
    console.log(activity)
  }
  descriptionclick =(activity) =>{
    this.props.navigation.navigate("Comment", { activity })

  }
  
  


  CustomActivity = (props) => {
    console.log("activity response ---->", props)
    console.log("activity id ---->", props.activity.id)
    console.log("comment  ---->", props.activity.reaction_counts.comment)

    const dateToFormat = props.activity.actor.created_at;
    let h = moment(dateToFormat, 'YYYY-MM-DD HH:mm:ss').fromNow();

    return (
      <View style={{ marginTop: 15, marginHorizontal: 10, backgroundColor: "#ffffff", borderRadius: 10 }}>
        <Activity
          {...props}
          onPress={()=> this.descriptionclick(props.activity)}

          
          Header={
            <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'white', paddingHorizontal: 10 }} >
              <Image source={{ uri: props.activity.actor.data.profileImage }} style={{ width: 50, height: 50, borderRadius: 25 }} />
              <View style={{ flex: 1, flexDirection: 'column', marginLeft: 8, marginTop: 6 }}>
                <Text style={{ fontSize: 18, color: '#000000', fontWeight: 'bold', fontStyle: 'normal'}}>{props.activity.actor.data.name}</Text>
                <Text style={{ fontSize: 14, fontWeight: "normal", color: '#757575' }}>{h}</Text>
              </View>
              <TouchableOpacity style={{ width: 50, height: 50, alignItems: 'flex-end', marginTop: 10 }}
                onPress={() => this.reportview()}>
                <Report name="dots-three-horizontal" size={20} color="#000000" />
              </TouchableOpacity>
              {/* <FollowButton styles={{ button: { width: 80 } }} /> */}

            </View>
          }
          

          Footer={
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <LikeButton {...props} />
              <TouchableOpacity
                style={{ width: 50, height: 50, paddingTop: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}
                onPress={() => this.commentclick(props.activity)}>
                <Icon name="comment" size={28} color="#757575" />
                <Text>{props.activity.reaction_counts.comment}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ width: 50, height: 50, paddingTop: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}
                onPress={() => this.commentclick(props.activity)}>
                <ShareIcon name="share" size={22} color="#757575" />
          <Text>{props.activity.reaction_counts.repost}</Text>
              </TouchableOpacity>


            </View>

          }
        />
      </View>
    );

  };
  render() {
    return (

      <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }} >


        <StreamApp
          apiKey="5rqsbgqvqphs"
          appId="40273"
          userId="48b97aaa-6bca-4497-8b95-112814e1a7cb"
          token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiNDhiOTdhYWEtNmJjYS00NDk3LThiOTUtMTEyODE0ZTFhN2NiIn0.-2kdbss-8amdQKYaY_KkzWbi-f1OAx0RpZFfTzvws40"
        >


          <FlatFeed
            feedGroup="timeline"
            userId="48b97aaa-6bca-4497-8b95-112814e1a7cb"
            Activity={this.CustomActivity}
            notify={true}

          />



        </StreamApp>


      </SafeAreaView>

    );
  }

}


