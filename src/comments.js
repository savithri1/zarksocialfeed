import React, { Component } from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

import { CommentList, CommentBox, SinglePost, StreamApp, Activity, updateStyle, LikeList, LikeButton, ReactionIcon, CommentItem } from 'react-native-activity-feed';
import Icon from 'react-native-vector-icons/dist/EvilIcons';


export default class Comments extends Component {
  constructor(props, context) {
    super(props, context);


    updateStyle('reactionIcon',{
      container:{
        marginLeft:10,
        paddingTop: 10,
        paddingBottom: 0,
        paddingLeft:0,
        paddingRight:0,
        alignItems: 'center',
        alignSelf: "center",
        height: 50,
      }

    })
    updateStyle('commentBox',{
        textInput:{
            flex :1,
            color: "#364047"

        },container:{
            flex :1,
            backgroundColor :'pink'
        }

    })
    

    this.state = {
    }
  }

  render() {
    const { navigation } = this.props;
    const activity = navigation.getParam('activity',"default");

    const feedGroup = activity.feedGroup
    const userId = activity.userId

    console.log("Single post ---->", navigation)
    console.log("activity  comment description---->", activity)
    console.log("feed  ---->", activity.feedGroup)
    console.log("userid ---->", activity.userId)
    console.log("userid ---->", activity.profileImage)


    // console.log('comment description',this.props)

    return (


      <SafeAreaView style={styles.container}>
        <StreamApp
          apiKey="5rqsbgqvqphs"
          appId="40273"
          userId="48b97aaa-6bca-4497-8b95-112814e1a7cb"
          token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiNDhiOTdhYWEtNmJjYS00NDk3LThiOTUtMTEyODE0ZTFhN2NiIn0.-2kdbss-8amdQKYaY_KkzWbi-f1OAx0RpZFfTzvws40">
          <SinglePost
            activity={activity}
            feedGroup={feedGroup}
            userId={userId}
            options={{ withOwnChildren: true }}
            navigation={this.props.navigation}
            Activity={(props) => (
              <React.Fragment>
               
                 <View style={styles.likesContainer}>
              </View>

                <CommentList
                  activityId={props.activity.id}
                  infiniteScroll
                  reverseOrder
                  CommentItem={({ comment }) => (
                    <React.Fragment>
                      <CommentItem
                        comment={comment}
                        // Footer={<LikeButton reaction={comment} {...props} />}
                      />
                    </React.Fragment>
                  )}
                />

                <View style={styles.sectionHeader} />
              </React.Fragment>
            )}
            Footer={(props) => (
              <CommentBox
                activity={activity}
                onAddReaction={props.onAddReaction}
                styles={{ container: { height: 78 } }}
                avatarProps={{
                    // source: props.activity.acor.data.profileImage,
                  }}
              />
            )}
          />
        </StreamApp>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
});