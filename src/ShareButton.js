import React from 'react';

import { StreamApp } from 'react-native-activity-feed';

import ShareButtonInner from '../src/ShareButtonInner'


export default class ShareButton extends React.Component {

    render() {
        return (
            <StreamApp.Consumer>
                {(appCtx) => {
                    return <ShareButtonInner activity={this.props.activity} {...this.props} {...appCtx} />;
                }}
            </StreamApp.Consumer>
        );
    }

}