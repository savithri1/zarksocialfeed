import React, { Component } from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

import { CommentList, CommentBox, SinglePost, StreamApp, Activity, updateStyle, LikeList, LikeButton, ReactionIcon, CommentItem } from 'react-native-activity-feed';
import Icon from 'react-native-vector-icons/dist/EvilIcons';


export default class Description extends Component {
  constructor(props, context) {
    super(props, context);


    updateStyle('reactionIcon',{
      container:{
        marginLeft:10,
        paddingTop: 10,
        paddingBottom: 0,
        paddingLeft:0,
        paddingRight:0,
        alignItems: 'center',
        alignSelf: "center",
        height: 50,
      }

    })
    updateStyle('likeButton', {
      text: {
        color: 'black'
      },
      container: {
        marginLeft: 10,
        paddingTop: 10,
        paddingBottom: 0,
        alignItems: 'center',
        height: 50,
        width:60
      },
      image: {
        width: 30,
        height: 30
      }

    })

    this.state = {
    }
  }

  render() {
    const { navigation } = this.props;
    const activity = navigation.getParam('activity',"default");

    const feedGroup = activity.feedGroup
    const userId = activity.userId

    console.log("Single post ---->", navigation)
    console.log("activity  comment description---->", activity)
    console.log("feed  ---->", activity.feedGroup)
    console.log("userid ---->", activity.userId)

    // console.log('comment description',this.props)

    return (


      <SafeAreaView style={styles.container}>
        <StreamApp
          apiKey="5rqsbgqvqphs"
          appId="40273"
          userId="48b97aaa-6bca-4497-8b95-112814e1a7cb"
          token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiNDhiOTdhYWEtNmJjYS00NDk3LThiOTUtMTEyODE0ZTFhN2NiIn0.-2kdbss-8amdQKYaY_KkzWbi-f1OAx0RpZFfTzvws40">
          <SinglePost
            activity={activity}
            feedGroup={feedGroup}
            userId={userId}
            options={{ withOwnChildren: true }}
            navigation={this.props.navigation}
            Activity={(props) => (
              <React.Fragment>
                <Activity
                  {...props}
                  Footer={
                    <View style={{ flex :1,flexDirection :'row'}}>
                      <LikeButton reactionKind="like" {...props} />
                      <View>
                      <ReactionIcon
                        // labelSingle="comment"
                        // labelPlural="comments"
                        icon={Icon}
                        counts={props.activity.reaction_counts}
                        kind="comment"
                      />
                      </View>
                    </View>
                  }
                />
                 <View style={styles.likesContainer}>
              </View>
              {/* <LikeList activityId={props.activity.id} reactionKind="like" /> */}

                <CommentList
                  activityId={props.activity.id}
                  infiniteScroll
                  reverseOrder
                  CommentItem={({ comment }) => (
                    <React.Fragment>
                      <CommentItem
                        comment={comment}
                        // Footer={<LikeButton reaction={comment} {...props} />}
                      />
                    </React.Fragment>
                  )}
                />

                <View style={styles.sectionHeader} />
              </React.Fragment>
            )}
            Footer={(props) => (
              <CommentBox
                activity={activity}
                onAddReaction={props.onAddReaction}
                avatarProps={{
                  // source: (userData: UserResponse) => userData.data.profileImage,
                }}
                styles={{ container: { height: 78 } }}
              />
            )}
          />
        </StreamApp>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
});