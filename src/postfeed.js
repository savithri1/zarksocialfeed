

import React, { Component } from 'react';
import {
    View,
    Dimensions,
} from 'react-native';
const { width, height } = Dimensions.get('window')
import { StreamApp, FlatFeed, StatusUpdateForm, updateStyle, FollowButton, Activity, LikeButton, CommentList, LikesList, SinglePost } from 'react-native-activity-feed';
import ShareButton from '../src/ShareButton';
const activity = {
    id: 'a727d86e-aa95-11e8-9d38-1231d51167b4',
};
export default class Postfeed extends Component {

    constructor(props) {
        super(props);

        updateStyle('statusUpdateForm', {
            container:{
                shadowOffset: {"width":0,"height":-3},
                shadowColor: "yellow",
                shadowOpacity: 0.1,
                backgroundColor: "green",
            },
           
            containerFocused:{
                height: 120

            },
            containerFocusedOg:{
                height: 171

            },
            newPostContainer:{
                backgroundColor: "pink",
                flexDirection: "column",
            },
           
            textInput:{
                padding: 10,
                marginRight: 10,
                backgroundColor: "#f8f8f8",
                borderRadius: 10
            },
            
            actionPanel:{
                justifyContent: "center"

            },
            actionPanelBlur:{
                flexDirection: "row",
                alignItems: "center"
            },
            accessory:{
                borderTopColor: "#DADFE3",
                backgroundColor: "#f6f6f6",
                borderTopWidth: 1,
                width: "100%",
                padding: 15,
                flexDirection: "row",
                alignItems: "center",
            },
                imageContainer:{

                
                width: 30,
                height: 30,
                overflow: "hidden",
                borderRadius: 4,
                alignItems: "center",
                justifyContent: "center",
                },
                imageContainerBlur:{
                marginRight: 8
            },
            
            imageOverlay:{
                position: "absolute",
                justifyContent: "center",
                alignItems: "center",
                width: 30,
                height: 30,
                padding: 8,
                backgroundColor: "rgba(0,0,0,0.4)"
            },
            
            image:{
                width: 30,
                height: 30
            },
            
            image_loading:{
                position: "absolute",
                width: 30,
                height: 30,
                opacity: 0.5
            },
            
            submitImage:{
                width: 24,
                height: 24
            }
            
               
        });


    }
  



    render() {
        
        return (

            <View style={{ flex: 1 }}>
                <StreamApp
                    apiKey="5rqsbgqvqphs"
                    appId="40273"
                    token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiN2ViZGRkZDYtMzM3OC00NGRmLWIxOTgtN2Y0ODFiNTljNTNkIn0.pboksf692G5SodjjTH7l-RJ-94DjOTiqSaek538lJ4w">

                     <StatusUpdateForm
                        fullscreen ={true}                    
                        doRequest={(submitFunc) => {
                            console.log('submitFunc', submitFunc)
                        }}
                        modifyActivityData={(data) => ({ ...data, target: 'Group:1' })}
                        onSuccess={(resp) => console.log('submitFunc', resp)}
                    /> 
       
          </StreamApp>
            </View>
        );
    }
}

