
import React, { Component } from 'react';
import {createAppContainer} from 'react-navigation';
import {
  SafeAreaView,
  View,
} from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';

import Socialfeed from './src/socialfeed';
import Description from './src/description';
import Postfeed from './src/postfeed';
import Comments from './src/comments';
const AppNavigator = createStackNavigator({
  Social: Socialfeed,
  Comment: Description,
  Post :Postfeed,
  List :Comments
});



const App = createAppContainer(AppNavigator);

export default App;

